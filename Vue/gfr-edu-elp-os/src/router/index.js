import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/login/Login.vue'
import StudentManage from "@/views/student_manage/StudentManage";
import ClassManage from "@/views/class_manage/ClassManage";
import CourseManage from "@/views/course_manage/CourseManage";
import LiveManage from "@/views/live_manage/LiveManage";
import NotFound from "@/components/layout/404";


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/student',
    name: 'StudentManage',
    component: StudentManage
  },
  {
    path: '/class',
    name: 'ClassManage',
    component: ClassManage
  },
  {
    path:'/course',
    name: 'CourseManage',
    component: CourseManage
  },
  {
    path:'/live',
    name: 'LiveManage',
    component: LiveManage
  },
  {
    path:'*',
    name: '404',
    component: NotFound
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to,from,next) => {
  const token = localStorage.getItem("access_token");

  if(to.name === 'Login'){
    next();
  }else{
    if (token === null || token === '') {
      Vue.prototype.$message("请先登录");
      next('/login');
    } else {
      next();
    }
  }
})
export default router
