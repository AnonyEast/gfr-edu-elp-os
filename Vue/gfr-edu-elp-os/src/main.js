import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/display.css';
import App from './App.vue'
import router from './router'
import store from './store'
import VueParticles from "vue-particles";
import axios from "axios";
import api from "@/common";
import AFTableColumn from 'af-table-column'

Vue.use(AFTableColumn)
Vue.use(ElementUI)
Vue.use(VueParticles)
Vue.config.productionTip = false
Vue.prototype.$ajax = axios; //配置ajax请求发送工具
Vue.prototype.$server = api.linux_api; //配置后台地址

// 添加请求拦截器，在请求头中加token
axios.interceptors.request.use(
    config => {
        if (localStorage.getItem('access_token')) {
            config.headers.Authorization = localStorage.getItem('access_token');
        }
        return config;
    },
    error => {
        return Promise.reject(error);
    });

// 添加响应拦截器，判断返回状态
axios.interceptors.response.use(
    res => {
        //对响应数据做些事
        return res
    },
    error => {
        if (error.response.status === 401) {
            // 401 说明 token 验证失败
            // 提示用户登录过期，直接跳转到登录页面
            localStorage.removeItem("access_token");
            Vue.prototype.$message("您的登录已经失效，请重新登录");
            router.push('/login').catch(error => {
                console.log('您已回到登录页面',error)
            });
        } else if (error.response.status === 500) {
            // 服务器错误
            // do something
            return Promise.reject(error.response.data)
        }
        // 返回 response 里的错误信息
        return Promise.reject(error.response.data)
    })

new Vue({
    router,
    store,
    render: function (h) {
        return h(App)
    }
}).$mount('#app')