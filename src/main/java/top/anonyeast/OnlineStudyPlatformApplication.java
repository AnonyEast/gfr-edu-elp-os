package top.anonyeast;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("top.anonyeast.dao")
@SpringBootApplication
public class OnlineStudyPlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineStudyPlatformApplication.class, args);
    }

}
