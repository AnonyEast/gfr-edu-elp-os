package top.anonyeast.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import top.anonyeast.dao.AccountDao;
import top.anonyeast.pojo.AccountInfo;
import top.anonyeast.service.UserService;

import javax.annotation.Resource;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource(name = "accountDao")
    private AccountDao dao;

    @Override
    public boolean userLogin(AccountInfo account) {
        QueryWrapper<AccountInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("username",account.getUsername());
        wrapper.eq("password",account.getPassword());
        return dao.selectCount(wrapper) > 0;
    }

    @Override
    public Long getUserCount(String username) {
        QueryWrapper<AccountInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        return dao.selectCount(wrapper);
    }

    @Override
    public boolean userSignup(AccountInfo account) {
        int insert_count = dao.insert(account);
        if (insert_count > 0){
            return true;
        }else {
            return false;
        }
    }
}
