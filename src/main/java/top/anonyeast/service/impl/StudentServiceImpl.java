package top.anonyeast.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import top.anonyeast.dao.StudentDao;
import top.anonyeast.pojo.Student;
import top.anonyeast.service.StudentService;

import javax.annotation.Resource;
import java.util.List;

@Service("studentService")
public class StudentServiceImpl implements StudentService {
    @Resource(name = "studentDao")
    private StudentDao dao;

    /**
     * 获取总条数
     * @return
     */
    @Override
    public Long getCount() {
        return dao.selectCount(null);
    }

    /**
     * 分页获取信息
     * @param page_num
     * @param page_size
     * @return
     */
    @Override
    public List<Student> getByPageNum(int page_num, int page_size) {
        //Oracle调用这一句
        //return dao.getByPageNum(page_num, page_size);
        //MySQL执行以下代码
        QueryWrapper<Student> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("CLASS_NAME","STU_NUMBER");
        Page<Student> studentPage = new Page<>(page_num,page_size);
        Page<Student> page = dao.selectPage(studentPage, wrapper);
        return page.getRecords();
    }

    /**
     * 根据关键词获取总条数
     * @param keywords
     * @return
     */
    @Override
    public Long getCountWithKeywords(String keywords) {
        //Oracle调用这一句
        //return dao.getCountWithKeywords(keywords);
        //MySQL执行以下代码
        QueryWrapper<Student> wrapper = new QueryWrapper<>();
        wrapper.like("PROFESSION",keywords).or()
                .like("CLASS_NAME",keywords).or()
                .like("STU_NUMBER",keywords).or()
                .like("STU_NAME",keywords).or()
                .like("STU_ID_NUMBER",keywords).or()
                .like("CULTURE_LEVEL",keywords).or()
                .like("SYSTEM_DESCRIPTION",keywords).or()
                .like("REMARKS",keywords).or()
                .like("DEPARTMENT",keywords).or()
                .eq("GRADE",keywords).or()
                .eq("ADMISSION_GRADE",keywords).or()
                .eq("BELONG_GRADE",keywords).or()
                .eq("EDUCATIONAL_SYSTEM",keywords).or()
                .eq("EDUCATIONAL_TYPE",keywords).or()
                .eq("STATUS",keywords).or()
                .eq("GENDER",keywords);
        return dao.selectCount(wrapper);
    }

    /**
     * 根据关键字分页获取信息
     * @param keywords
     * @param page_num
     * @param page_size
     * @return
     */
    @Override
    public List<Student> getByPageNumWithKeywords(String keywords, int page_num, int page_size) {
        //Oracle调用这一句
        //return dao.getByPageNumWithKeywords(keywords, page_num, page_size);
        //MySQL
        QueryWrapper<Student> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("CLASS_NAME","STU_NUMBER")
                .like("PROFESSION",keywords).or()
                .like("CLASS_NAME",keywords).or()
                .like("STU_NUMBER",keywords).or()
                .like("STU_NAME",keywords).or()
                .like("STU_ID_NUMBER",keywords).or()
                .like("CULTURE_LEVEL",keywords).or()
                .like("SYSTEM_DESCRIPTION",keywords).or()
                .like("REMARKS",keywords).or()
                .like("DEPARTMENT",keywords).or()
                .eq("GRADE",keywords).or()
                .eq("ADMISSION_GRADE",keywords).or()
                .eq("BELONG_GRADE",keywords).or()
                .eq("EDUCATIONAL_SYSTEM",keywords).or()
                .eq("EDUCATIONAL_TYPE",keywords).or()
                .eq("STATUS",keywords).or()
                .eq("GENDER",keywords);
        Page<Student> studentPage = new Page<>(page_num,page_size);
        Page<Student> page = dao.selectPage(studentPage, wrapper);
        return page.getRecords();
    }

    /**
     * 根据学号删除
     * @param stuNumber
     * @return
     */
    @Override
    public boolean deleteByStuNumber(String stuNumber) {
        QueryWrapper<Student> wrapper = new QueryWrapper<>();
        wrapper.eq("stu_number", stuNumber);
        int delete_count = dao.delete(wrapper);
        System.out.println("删除了" + delete_count + "条记录");
        return delete_count > 0;
    }

    /**
     * 添加学生
     * @param student
     * @return
     */
    @Override
    public boolean insertStudent(Student student) {
        int affected_count = 0;//受影响的行数
        QueryWrapper<Student> wrapper = new QueryWrapper<>();
        //先判断学号存不存在
        wrapper.eq("stu_number", student.getStuNumber());
        Long stuNumber_count = dao.selectCount(wrapper);
        //若存在直接返回false
        if (stuNumber_count > 0){
            return false;
        }else {
            //不存在则插入
            affected_count = dao.insert(student);
        }
        return affected_count > 0;
    }

    @Override
    public boolean updateStudent(Student student) {
        int affected_count = 0;//受影响的行数
        QueryWrapper<Student> wrapper = new QueryWrapper<>();
        wrapper.eq("stu_number", student.getStuNumber());
        affected_count = dao.update(student,wrapper);
        return affected_count > 0;
    }
}
