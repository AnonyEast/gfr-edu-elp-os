package top.anonyeast.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import top.anonyeast.dao.CourseInfoDao;
import top.anonyeast.pojo.ClassInfo;
import top.anonyeast.pojo.CourseInfo;
import top.anonyeast.service.CourseService;

import javax.annotation.Resource;
import java.util.List;

@Service("courseService")
public class CourseServiceImpl implements CourseService {
    @Resource(name = "courseDao")
    private CourseInfoDao dao;

    /**
     *
     * @param page_num
     * @param page_size
     * @return
     */
    @Override
    public List<CourseInfo> getByPageNum(int page_num, int page_size) {
        QueryWrapper<CourseInfo> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("course_id");
        Page<CourseInfo> studentPage = new Page<>(page_num,page_size);
        Page<CourseInfo> page = dao.selectPage(studentPage, wrapper);
        return page.getRecords();
    }

    @Override
    public Long getCourseCount() {
        return dao.selectCount(null);
    }

    @Override
    public Long getCountWithKeywords(String keywords) {
        QueryWrapper<CourseInfo> wrapper = new QueryWrapper<>();
        wrapper.like("course_id",keywords).or()
                .like("course_name",keywords).or()
                .like("course_type",keywords).or()
                .eq("course_price",keywords);
        return dao.selectCount(wrapper);
    }

    /**
     * 根据关键词获取总条数
     * @return
     */
    @Override
    public List<CourseInfo> getByPageNumWithKeywords(String keywords, int page_num, int page_size) {
        QueryWrapper<CourseInfo> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("course_id")
                .like("course_id",keywords).or()
                .like("course_name",keywords).or()
                .like("course_type",keywords).or()
                .eq("course_price",keywords);
        Page<CourseInfo> classPage = new Page<>(page_num,page_size);
        Page<CourseInfo> page = dao.selectPage(classPage, wrapper);
        return page.getRecords();
    }

    @Override
    public boolean deleteByCourseId(String courseId) {
        QueryWrapper<CourseInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId);
        int delete_count = dao.delete(wrapper);
        System.out.println("删除了" + delete_count + "条记录");
        return delete_count > 0;
    }

    @Override
    public boolean insertCourse(CourseInfo course) {
        return dao.insert(course) > 0;
    }

    @Override
    public boolean updateCourse(CourseInfo course) {
        int affected_count = 0;//受影响的行数
        QueryWrapper<CourseInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", course.getCourseId());
        affected_count = dao.update(course,wrapper);
        return affected_count > 0;
    }
}
