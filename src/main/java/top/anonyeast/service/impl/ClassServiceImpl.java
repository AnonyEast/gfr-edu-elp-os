package top.anonyeast.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;


import top.anonyeast.dao.ClassDao;
import top.anonyeast.pojo.ClassInfo;

import top.anonyeast.pojo.Student;
import top.anonyeast.service.ClassService;

import javax.annotation.Resource;
import java.util.List;

@Service("classService")
public class ClassServiceImpl implements ClassService {
    @Resource(name = "classDao")
    private ClassDao dao;

    @Override
    public List<ClassInfo> getByPageNum(int page_num, int page_size) {
        QueryWrapper<ClassInfo> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("department","class_name");
        Page<ClassInfo> studentPage = new Page<>(page_num,page_size);
        Page<ClassInfo> page = dao.selectPage(studentPage, wrapper);
        return page.getRecords();
    }

    @Override
    public Long getClassCount() {
        return dao.selectCount(null);
    }

    @Override
    public Long getCountWithKeywords(String keywords) {
        QueryWrapper<ClassInfo> wrapper = new QueryWrapper<>();
        wrapper.like("class_name",keywords).or()
                .like("department",keywords).or()
                .like("class_manager",keywords).or()
                .like("class_remarks",keywords);
        return dao.selectCount(wrapper);
    }

    @Override
    public List<ClassInfo> getByPageNumWithKeywords(String keywords, int page_num, int page_size) {
        QueryWrapper<ClassInfo> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("department","class_name")
                .like("class_name",keywords).or()
                .like("department",keywords).or()
                .like("class_manager",keywords).or()
                .like("class_remarks",keywords);
        Page<ClassInfo> classPage = new Page<>(page_num,page_size);
        Page<ClassInfo> page = dao.selectPage(classPage, wrapper);
        return page.getRecords();
    }

    @Override
    public boolean deleteByClassId(String classId) {
        QueryWrapper<ClassInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("class_name", classId);
        int delete_count = dao.delete(wrapper);
        System.out.println("删除了" + delete_count + "条记录");
        return delete_count > 0;
    }

    @Override
    public boolean insertClass(ClassInfo classInfo) {
        int affected_count = 0;//受影响的行数
        QueryWrapper<ClassInfo> wrapper = new QueryWrapper<>();
        //先判断学号存不存在
        wrapper.eq("class_name", classInfo.getClassName());
        Long className_count = dao.selectCount(wrapper);
        //若存在直接返回false
        if (className_count > 0){
            return false;
        }else {
            //不存在则插入
            affected_count = dao.insert(classInfo);
        }
        return affected_count > 0;
    }

    @Override
    public boolean updateClass(ClassInfo classInfo) {
        int affected_count = 0;//受影响的行数
        QueryWrapper<ClassInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("class_name", classInfo.getClassName());
        affected_count = dao.update(classInfo,wrapper);
        return affected_count > 0;
    }
}
