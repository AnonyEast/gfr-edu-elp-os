package top.anonyeast.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import top.anonyeast.dao.AccountDao;
import top.anonyeast.dao.LiveInfoDao;
import top.anonyeast.pojo.AccountInfo;
import top.anonyeast.pojo.LiveInfo;
import top.anonyeast.service.LiveService;

import javax.annotation.Resource;
import java.util.List;

@Service("liveService")
public class LiveServiceImpl implements LiveService {
    @Resource(name = "liveInfoDao")
    private LiveInfoDao dao;


    @Override
    public List<LiveInfo> getByPageNum(int page_num, int page_size) {
        return null;
    }

    @Override
    public boolean delete(String live_id) {
        return false;
    }

    @Override
    public boolean insert(LiveInfo live) {
        return false;
    }

    @Override
    public boolean update(LiveInfo live) {
        return false;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public int getCountWithKeywords(String keywords) {
        return 0;
    }

    @Override
    public List<LiveInfo> getByPageNumWithKeywords(String keywords, int page_num, int page_size) {
        return null;
    }
}
