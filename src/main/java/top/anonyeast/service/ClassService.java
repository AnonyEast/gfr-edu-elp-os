package top.anonyeast.service;

import top.anonyeast.pojo.ClassInfo;

import java.util.List;

public interface ClassService {
    List<ClassInfo> getByPageNum(int page_num, int page_size);
    Long getClassCount();
    Long getCountWithKeywords(String keywords);
    List<ClassInfo> getByPageNumWithKeywords(String keywords,int page_num,int page_size);
    boolean deleteByClassId(String classId);
    boolean insertClass(ClassInfo classInfo);
    boolean updateClass(ClassInfo classInfo);


}
