package top.anonyeast.service;

import top.anonyeast.pojo.LiveInfo;

import java.util.List;

public interface LiveService {
    public List<LiveInfo> getByPageNum(int page_num,int page_size);
    public boolean delete(String live_id);
    public boolean insert(LiveInfo live);
    public boolean update(LiveInfo live);
    public int getCount();
    public int getCountWithKeywords(String keywords);
    public List<LiveInfo> getByPageNumWithKeywords(String keywords,int page_num,int page_size);
}
