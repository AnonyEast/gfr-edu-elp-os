package top.anonyeast.service;

import top.anonyeast.pojo.AccountInfo;

public interface UserService {
    boolean userLogin(AccountInfo account);
    Long getUserCount(String username);
    boolean userSignup(AccountInfo account);
}
