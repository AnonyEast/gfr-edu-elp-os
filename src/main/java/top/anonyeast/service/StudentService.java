package top.anonyeast.service;

import top.anonyeast.pojo.Student;

import java.util.List;

public interface StudentService {
    List<Student> getByPageNum(int page_num,int page_size);
    Long getCount();
    Long getCountWithKeywords(String keywords);
    List<Student> getByPageNumWithKeywords(String keywords,int page_num,int page_size);
    boolean deleteByStuNumber(String stuNumber);
    boolean insertStudent(Student student);
    boolean updateStudent(Student student);
}
