package top.anonyeast.service;

import top.anonyeast.pojo.CourseInfo;

import java.util.List;

public interface CourseService {
    List<CourseInfo> getByPageNum(int page_num, int page_size);
    Long getCourseCount();
    Long getCountWithKeywords(String keywords);
    List<CourseInfo> getByPageNumWithKeywords(String keywords,int page_num,int page_size);
    boolean deleteByCourseId(String courseId);
    boolean insertCourse(CourseInfo course);
    boolean updateCourse(CourseInfo course);
}
