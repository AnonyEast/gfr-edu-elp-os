package top.anonyeast.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.anonyeast.config.Auth;
import top.anonyeast.pojo.AccountInfo;
import top.anonyeast.service.UserService;
import top.anonyeast.util.JwtUtil;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {
    @Resource(name = "userService")
    private UserService service;

    @PostMapping("/login")
    public boolean login(AccountInfo account) {
        System.out.println(account);
        return service.userLogin(account);
    }

    @PostMapping("/jwtLogin")
    public Map<String, String> jwtLogin(AccountInfo account) {
        HashMap<String, String> map = new HashMap<>();
        if (service.userLogin(account)) {
            String token = JwtUtil.createToken(Auth.JWT_SEC, Auth.EXPIRE, account.getUsername());
            map.put("status", "success");
            map.put("token", token);
            return map;
        }else {
            map.put("status","failed");
            return map;
        }
    }

    @PostMapping("/getUserCount")
    public Long getUserCount(String username){
        return service.getUserCount(username);
    }

    @PostMapping("/signup")
    public boolean signup(AccountInfo account) {
        return service.userSignup(account);
    }
}
