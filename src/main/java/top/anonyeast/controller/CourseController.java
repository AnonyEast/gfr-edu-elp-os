package top.anonyeast.controller;

import org.springframework.web.bind.annotation.*;
import top.anonyeast.pojo.CourseInfo;
import top.anonyeast.service.CourseService;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/course")
public class CourseController {
    @Resource(name = "courseService")
    CourseService service;

    @PostMapping("/getCount")
    public Long getCourseCount(String keywords){
        System.out.println("获取总条数：");
        System.out.println("keywords = " + keywords);
        if (keywords == null || keywords.length() == 0 || "undefined".equals(keywords)){
            return service.getCourseCount();
        }
        return service.getCountWithKeywords(keywords);
    }
    @PostMapping("/getByPageNum")
    public List<CourseInfo> getByPageNum(String keywords, int page_num, int page_size){
        System.out.println("查询：");
        System.out.println("keywords = " + keywords);
        System.out.println("page_num = " + page_num);
        System.out.println("page_size = " + page_size);
        if (keywords == null || keywords.length() == 0 || "undefined".equals(keywords)){
            return service.getByPageNum(page_num,page_size);
        }
        return service.getByPageNumWithKeywords(keywords,page_num,page_size);
    }

    @PostMapping("/addViewTimes")
    public boolean addViewTimes(CourseInfo course){
        System.out.println("增加访问量：");
        System.out.println(course);
        course.setCourseViewTimes(course.getCourseViewTimes() + 1);
        return service.updateCourse(course);
    }

    @PostMapping("/deleteByCourseId")
    public boolean deleteByCourseId(String courseId){
        System.out.println("删除：");
        System.out.println("courseId = " + courseId);
        return service.deleteByCourseId(courseId);
    }

    @PostMapping("/insert")
    public boolean insertCourse(@RequestBody CourseInfo course){
        System.out.println("添加：");
        System.out.println(course);
        return service.insertCourse(course);
    }

    @PostMapping("/modify")
    public boolean updateCourse(@RequestBody CourseInfo course){
        System.out.println("修改：");
        System.out.println(course);
        return service.updateCourse(course);
    }
}
