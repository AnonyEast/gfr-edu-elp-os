package top.anonyeast.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.anonyeast.dao.RoleDao;
import top.anonyeast.pojo.AccountInfo;
import top.anonyeast.pojo.Role;
import top.anonyeast.service.UserService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController {
    @Resource(name = "roleDao")
    private RoleDao dao;

    @PostMapping("/getRole")
    public List<String> login(AccountInfo account){
        List<Role> roles = dao.selectList(null);
        ArrayList<String> roleListStr = new ArrayList<>();
        for (Role role : roles) {
            roleListStr.add(role.getRoleName());
        }
        return roleListStr;
    }
}
