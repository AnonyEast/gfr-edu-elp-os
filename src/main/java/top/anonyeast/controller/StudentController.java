package top.anonyeast.controller;

import org.springframework.web.bind.annotation.*;
import top.anonyeast.pojo.Student;
import top.anonyeast.service.StudentService;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {
    @Resource(name = "studentService")
    StudentService service;

    @PostMapping("/getCount")
    public Long getCount(String keywords){
        System.out.println("获取总条数：");
        System.out.println("keywords = " + keywords);
        if (keywords == null || keywords.length() == 0 || "undefined".equals(keywords)){
            return service.getCount();
        }
        return service.getCountWithKeywords(keywords);
    }

    @PostMapping("/getByPageNum")
    public List<Student> getByPageNum(String keywords,int page_num,int page_size){
        System.out.println("查询：");
        System.out.println("keywords = " + keywords);
        System.out.println("page_num = " + page_num);
        System.out.println("page_size = " + page_size);
        if (keywords == null || keywords.length() == 0 || "undefined".equals(keywords)){
            return service.getByPageNum(page_num,page_size);
        }
        return service.getByPageNumWithKeywords(keywords,page_num,page_size);
    }

    @PostMapping("/deleteByStuNumber")
    public boolean deleteByStuNumber(String stuNumber){
        System.out.println("删除：");
        System.out.println("stuNumber = " + stuNumber);
        return service.deleteByStuNumber(stuNumber);
    }

    @PostMapping("/insert")
    public boolean insertStudent(@RequestBody Student student){
        System.out.println("添加：");
        System.out.println(student);
        return service.insertStudent(student);
    }

    @PostMapping("/modify")
    public boolean updateStudent(@RequestBody Student student){
        System.out.println("修改：");
        System.out.println(student);
        return service.updateStudent(student);
    }
}
