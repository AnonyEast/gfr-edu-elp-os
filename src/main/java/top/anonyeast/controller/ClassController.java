package top.anonyeast.controller;

import org.springframework.web.bind.annotation.*;
import top.anonyeast.pojo.ClassInfo;
import top.anonyeast.service.ClassService;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/class")
public class ClassController {
    @Resource(name = "classService")
    ClassService service;

    @PostMapping("/getCount")
    public Long getCount(String keywords){
        System.out.println("获取总班级数目：");
        System.out.println("keywords = " + keywords);
        if (keywords == null || keywords.length() == 0 || "undefined".equals(keywords)){
            return service.getClassCount();
        }
        return service.getCountWithKeywords(keywords);
    }

    @PostMapping("/getByPageNum")
    public List<ClassInfo> getByPageNum(String keywords, int page_num, int page_size){
        System.out.println("查询：");
        System.out.println("keywords = " + keywords);
        System.out.println("page_num = " + page_num);
        System.out.println("page_size = " + page_size);
        if (keywords == null || keywords.length() == 0 || "undefined".equals(keywords)){
            return service.getByPageNum(page_num,page_size);
        }
        return service.getByPageNumWithKeywords(keywords,page_num,page_size);
    }

    @PostMapping("/deleteByClassId")
    public boolean deleteBYClassId(String className){
        System.out.println("删除：");
        System.out.println("ClassName = " + className);
        return service.deleteByClassId(className);
    }

    @PostMapping("/insert")
    public boolean insertClass(@RequestBody ClassInfo classInfo){
        System.out.println("添加：");
        System.out.println(classInfo);
        return service.insertClass(classInfo);
    }

    @PostMapping("/modify")
    public boolean updateClass(@RequestBody ClassInfo classInfo){
        System.out.println("修改：");
        System.out.println(classInfo);
        return service.updateClass(classInfo);
    }
}

