package top.anonyeast.controller;

import org.springframework.web.bind.annotation.*;
import top.anonyeast.pojo.AccountInfo;
import top.anonyeast.pojo.LiveInfo;
import top.anonyeast.service.LiveService;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/live")
public class LiveController {
    @Resource(name = "liveService")
    LiveService service;

    @PostMapping("/getCount")
    public int getCount(String keywords){
        System.out.println("获取总条数：");
        System.out.println("keywords = " + keywords);
        if (keywords == null || keywords.length() == 0 || "undefined".equals(keywords)){
            return service.getCount();
        }
        return service.getCountWithKeywords(keywords);
    }

    @PostMapping("/getByPageNum")
    public List<LiveInfo> getByPageNum(String keywords, int page_num, int page_size){
        System.out.println("查询：");
        System.out.println("keywords = " + keywords);
        System.out.println("page_num = " + page_num);
        System.out.println("page_size = " + page_size);
        if (keywords == null || keywords.length() == 0 || "undefined".equals(keywords)){
            return service.getByPageNum(page_num,page_size);
        }
        return service.getByPageNumWithKeywords(keywords,page_num,page_size);
    }

    @PostMapping("/deleteByLiveId")
    public boolean deleteByLiveId(String live_id){
        System.out.println("删除：");
        System.out.println("stuNumber = " + live_id);
        return service.delete(live_id);
    }

    @PostMapping("/insert")
    public boolean insert(@RequestBody LiveInfo live){
        System.out.println("添加：");
        System.out.println(live);
        return service.insert(live);
    }

    @PostMapping("/modify")
    public boolean update(@RequestBody LiveInfo live){
        System.out.println("修改：");
        System.out.println(live);
        return service.update(live);
    }
}
