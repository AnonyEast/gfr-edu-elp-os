package top.anonyeast.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.anonyeast.pojo.Department;
@Repository("departmentDao")
public interface DepartmentDao extends BaseMapper<Department> {
}
