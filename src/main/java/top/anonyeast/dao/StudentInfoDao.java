package top.anonyeast.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.anonyeast.pojo.Student;

@Repository("studentInfoDao")
public interface StudentInfoDao extends BaseMapper<Student> {
}
