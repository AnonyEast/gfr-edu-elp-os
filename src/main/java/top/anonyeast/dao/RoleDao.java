package top.anonyeast.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.anonyeast.pojo.LiveInfo;
import top.anonyeast.pojo.Role;

@Repository("roleDao")
public interface RoleDao extends BaseMapper<Role> {
}
