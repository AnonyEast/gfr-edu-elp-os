package top.anonyeast.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.anonyeast.pojo.AccountInfo;

@Repository("accountDao")
public interface AccountDao extends BaseMapper<AccountInfo> {
}
