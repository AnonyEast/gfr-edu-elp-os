package top.anonyeast.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import top.anonyeast.pojo.Student;

import java.util.List;

@Repository
public interface StudentDao extends BaseMapper<Student> {
}
