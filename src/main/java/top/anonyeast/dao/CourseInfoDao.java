package top.anonyeast.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import top.anonyeast.pojo.AccountInfo;
import top.anonyeast.pojo.CourseInfo;

import java.util.List;

@Repository("courseDao")
public interface CourseInfoDao extends BaseMapper<CourseInfo> {
    /**
     * 分页查询
     * @param page_num 页码
     * @param page_size 每页的条数
     * @return
     */
    List<CourseInfo> getByPageNum(int page_num, int page_size);

    /**
     * 搜索
     * @param page_num
     * @param page_size
     * @return
     */
    List<CourseInfo> getByPageNumWithKeywords(@Param("keywords") String keywords, @Param("page_num") int page_num, @Param("page_size") int page_size);

    /**
     * 根据关键词找总条数
     * @param keywords
     * @return
     */
    int getCountWithKeywords(String keywords);
}
