package top.anonyeast.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.anonyeast.pojo.CourseChapter;
import top.anonyeast.pojo.LiveInfo;

@Repository("liveInfoDao")
public interface LiveInfoDao extends BaseMapper<LiveInfo> {
}
