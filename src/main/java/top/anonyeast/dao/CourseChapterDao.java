package top.anonyeast.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.anonyeast.pojo.ClassInfo;
import top.anonyeast.pojo.CourseChapter;

@Repository("courseChapterDao")
public interface CourseChapterDao extends BaseMapper<CourseChapter> {
}