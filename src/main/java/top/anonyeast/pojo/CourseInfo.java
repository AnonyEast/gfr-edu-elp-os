package top.anonyeast.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("course_info")
public class CourseInfo {
    Integer courseId;
    String courseName;
    String courseType;
    Integer coursePrice;
    Integer courseViewTimes;
}
