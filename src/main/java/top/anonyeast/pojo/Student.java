package top.anonyeast.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("student_info")
public class Student {
    private Long id;
    private String department;
    private String grade;
    private String profession;
    private String className;
    private String stuNumber;
    private String stuName;
    private String gender;
    private String stuIdNumber;
    private String admissionGrade;
    private String belongGrade;
    private String educationalSystem;
    private String cultureLevel;
    private String educationalType;
    private String status;
    private String systemDescription;
    private String remarks;
}
