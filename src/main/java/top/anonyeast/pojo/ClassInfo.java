package top.anonyeast.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("class_info")
public class ClassInfo {
    String className;
    String department;
    String classManager;
    String classRemarks;
}
