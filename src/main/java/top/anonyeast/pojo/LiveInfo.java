package top.anonyeast.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("live_info")
public class LiveInfo {
    Integer liveId;
    String liveName;
    String liveStatus;
}
