package top.anonyeast.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("account_info")
public class AccountInfo {
    String username;//用户名
    String password;//密码
    String roleName;//角色名
    Date registTime;//注册时间
}
