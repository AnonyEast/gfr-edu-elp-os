package top.anonyeast.config;

public abstract class Auth {
    //JWT私钥
    public static final String JWT_SEC = "OnlineStudyPlatformApplication";
    //token过期时间(毫秒)
    public static final long EXPIRE = 1000 * 60 * 60 * 3;
}
