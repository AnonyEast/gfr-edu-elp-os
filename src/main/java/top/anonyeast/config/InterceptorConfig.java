package top.anonyeast.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import top.anonyeast.interceptor.CorsInterceptor;
import top.anonyeast.interceptor.JwtInterceptor;

import java.util.Arrays;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        String[] vueStaticResource = {"/css/**","/fonts/**","/img/**","/js/**","/favicon.ico","/index.html"};
        registry.addInterceptor(new CorsInterceptor())
                .addPathPatterns("/**");
        registry.addInterceptor(new JwtInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/user/**")
                .excludePathPatterns("/role/**")
                .excludePathPatterns("/")
                .excludePathPatterns(Arrays.asList(vueStaticResource));

    }
}
